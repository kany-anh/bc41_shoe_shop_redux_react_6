import "./App.css";
import Shoe_Shop_Redux from "./Shoe_Shoe_Redux/Shoe_Shop_Redux";

function App() {
  return (
    <div className="App">
      <Shoe_Shop_Redux />
    </div>
  );
}

export default App;
