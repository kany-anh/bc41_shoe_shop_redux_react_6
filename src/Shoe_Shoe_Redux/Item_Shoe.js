import React, { Component } from "react";
import { connect } from "react-redux";
import { ADD_TO_CART } from "./redux/constant/shoeConstant";

class Item_Shoe extends Component {
  render() {
    let { name, image, price } = this.props.data;
    return (
      <div className="card col-3">
        <img src={image} className="card-img-top" alt="" />
        <div className="card-body">
          <h5 className="card-title">{name}</h5>
          <p className="card-text">{price}</p>
          <button
            href="#"
            onClick={() => {
              this.props.handleAddToCart(this.props.data);
            }}
            className="btn btn-primary"
          >
            Add to cart
          </button>
        </div>
      </div>
    );
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    handleAddToCart: (Shoe) => {
      dispatch({
        type: ADD_TO_CART,
        payload: Shoe,
      });
    },
  };
};

export default connect(null, mapDispatchToProps)(Item_Shoe);
