import React, { Component } from "react";
import Cart_Shoe from "./Cart_Shoe";
import List_Shoe from "./List_Shoe";

class Shoe_Shop_Redux extends Component {
  render() {
    return (
      <div>
        <List_Shoe />
        <Cart_Shoe />
      </div>
    );
  }
}

export default Shoe_Shop_Redux;
