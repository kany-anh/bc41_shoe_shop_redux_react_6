import React, { Component } from "react";
import { connect } from "react-redux";
import { CHANGE_QUANTITY, DELETE_SHOE } from "./redux/constant/shoeConstant";

class Cart_Shoe extends Component {
  renderCart = () => {
    return this.props.cart.map((cartShoe, index) => {
      return (
        <tr key={index}>
          <td>{cartShoe.id}</td>
          <td>{cartShoe.name}</td>
          <td>{cartShoe.price * cartShoe.soLuong}</td>
          <td>
            <button
              onClick={() => {
                this.props.handleChangeAmout(cartShoe.id, -1);
              }}
              className="btn btn-warning mx-2"
            >
              -
            </button>
            {cartShoe.soLuong}
            <button
              onClick={() => {
                this.props.handleChangeAmout(cartShoe.id, 1);
              }}
              className="btn btn-success mx-2"
            >
              +
            </button>
          </td>
          <td>
            <img style={{ width: 50 }} src={cartShoe.image} alt="" />
          </td>
          <td>
            <button
              onClick={() => {
                this.props.handleDelete(cartShoe.id);
              }}
              className="btn btn-danger"
            >
              Xóa
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div className=" my-5">
        <table className="table">
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Price</th>
              <th>Amout</th>
              <th>Image</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>{this.renderCart()}</tbody>
        </table>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    cart: state.shoeReducer.cart,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleChangeAmout: (idShoe, luaChon) => {
      dispatch({
        type: CHANGE_QUANTITY,
        payload: { idShoe, luaChon },
      });
    },
    handleDelete: (idShoe) => {
      dispatch({
        type: DELETE_SHOE,
        payload: idShoe,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Cart_Shoe);
