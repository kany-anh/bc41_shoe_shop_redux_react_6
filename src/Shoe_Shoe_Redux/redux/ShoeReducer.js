import { data_shoe } from "../data_shoe";
import {
  ADD_TO_CART,
  CHANGE_QUANTITY,
  DELETE_SHOE,
} from "./constant/shoeConstant";

let initialValue = {
  list: data_shoe,
  cart: [],
};
export const shoeReducer = (state = initialValue, action) => {
  switch (action.type) {
    case ADD_TO_CART: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((shoe) => {
        return shoe.id === action.payload.id;
      });
      if (index === -1) {
        let newShoe = { ...action.payload, soLuong: 1 };
        cloneCart.push(newShoe);
      } else {
        cloneCart[index].soLuong++;
      }
      return { ...state, cart: cloneCart };
    }
    case CHANGE_QUANTITY: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((shoe) => {
        return shoe.id === action.payload.idShoe;
      });
      cloneCart[index].soLuong += action.payload.luaChon;
      return { ...state, cart: cloneCart };
    }
    case DELETE_SHOE: {
      let newCart = state.cart.filter((shoe) => {
        return shoe.id !== action.payload;
      });
      return { ...state, cart: newCart };
    }
    default:
      return state;
  }
};
