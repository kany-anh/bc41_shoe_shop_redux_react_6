import React, { Component } from "react";
import { connect } from "react-redux";
import Item_Shoe from "./Item_Shoe";

class List_Shoe extends Component {
  renderListShoe = () => {
    return this.props.list_shoe.map((item, index) => {
      return <Item_Shoe key={index} data={item} />;
    });
  };
  render() {
    return (
      <div className="container">
        <h2>List Shoe</h2>
        <div className="row my-2">{this.renderListShoe()}</div>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    list_shoe: state.shoeReducer.list,
  };
};

export default connect(mapStateToProps, null)(List_Shoe);
